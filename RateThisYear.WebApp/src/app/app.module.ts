import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modules
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { TakeStockComponent } from './pages/take-stock/take-stock.component';
import { EventListComponent } from './components/list/event-list/event-list.component';
import { HomeButtonComponent } from './components/button/home-button/home-button.component';
import { RateYearDialogBoxComponent } from './components/dialog-box/rate-year-dialog-box/rate-year-dialog-box.component';

// PrimeNg Components
import { FormsModule } from '@angular/forms';
import {ButtonModule} from 'primeng/button';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TakeStockComponent,
    EventListComponent,
    HomeButtonComponent,
    RateYearDialogBoxComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      positionClass: 'toast-top-center',
    }),
    FormsModule,
    ButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }