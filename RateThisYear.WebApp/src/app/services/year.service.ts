import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Year } from '../models/year';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class YearService {

  // Year Api base URL
  private yearApiUrl = `${environment.microservices.year.baseUrl}${environment.microservices.year.yearApi.baseUrl}`;

  constructor(private httpClient: HttpClient) { }

  // Get year by Id
  getYear(yearId: string): Observable<Year>{
    return this.httpClient.get<Year>(this.yearApiUrl + '/' + yearId);
  }

  // Posting new year to the database
  createNewYear(year: Year): Observable<Year>{
    return this.httpClient.post<Year>(this.yearApiUrl, year);
  }

}
