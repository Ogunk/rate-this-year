import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Year } from 'src/app/models/year';
import { YearService } from 'src/app/services/year.service';

@Component({
  selector: 'app-take-stock',
  templateUrl: './take-stock.component.html',
  styleUrls: ['./take-stock.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TakeStockComponent implements OnInit {

  @Input()
  isEditMode: boolean = true;

  @Input()
  showRateYearDialogBox: boolean = false;

  @Input()
  goodListIsFilled: boolean = false;

  @Input()
  badListIsFilled: boolean = false;

  @Input()
  showBadListAlertBox: boolean = false;

  @Input()
  showGoodListAlertBox: boolean = false;

  takeStockHeader: string = "What happened during your year ?";
  takeStockText: string = "Think about everything that happened during this year, " +
    "it could be about you, you started doing sport or reading, you learned new things " +
    "<br>or completed a project you had on your mind. And so on, family, friends, health, financials etc …";

  introspectionHeader: string = "Introspection";
  introspectionText: string = `I hope this (shitty) year was not as bad as you thought it was and if it was...` +
    `I wish you the best for the next year and hope this site helped you. ` +
    `<br>Hard times won't last and good times too. Please consider rating this year by clicking <span class="rate-this-year">here.</span>`;

  constructor(
    private yearService: YearService,
    private toastr: ToastrService
    ) { }

  ngOnInit(): void {
  }

  onRateAndGenerateClick(value: boolean) {
    
    if(!this.badListIsFilled){
      this.handleAlertBoxVisibilty(value, "badList");
    }

    if(!this.goodListIsFilled){
      this.handleAlertBoxVisibilty(value, "goodList");
    }

    if (this.isEditMode && (this.badListIsFilled && this.goodListIsFilled)) {
      this.showRateYearDialogBox = value;
    } else {
      this.isEditMode = true;
    }
  }

  onGenerateClick(value: boolean) {
    this.isEditMode = value;
  }

  badListIsFilledEvent(value: boolean){
    if(!this.badListIsFilled) this.badListIsFilled = value;
  }

  goodListIsFilledEvent(value: boolean){
    if(!this.goodListIsFilled) this.goodListIsFilled = value;
  }

  handleAlertBoxVisibilty(value: boolean, listName: string){
    if(listName === "badList"){
      this.showBadListAlertBox = value;
      this.toastr.error("The bad list events can't be empty", "Bad list events error");
    }

    if(listName === "goodList"){
      this.showGoodListAlertBox = value;
      this.toastr.error("The good list events can't be empty", "Good list events error");
    }
  }

  onShareClick(){
    console.log('share this baby');
  }

}
