import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {

  @Input() goodList: boolean = true;
  @Input() isEditMode: boolean = true;

  @Output() listIsFilled = new EventEmitter<boolean>();

  events: string[] = [];

  userInputValue:string = "";

  constructor() { }

  ngOnInit(): void {
  }

  onKeyUp(event: any){
    this.userInputValue = event.target.value;
  }

  onAddClick(){
    if(this.userInputValue.length > 0){
      this.events.push(this.userInputValue);
      this.listIsFilled.emit(true);
      this.userInputValue = "";
    }
  }

  onRemoveClick(event: any){
    this.events.splice(event, 1);
  }
  
}
