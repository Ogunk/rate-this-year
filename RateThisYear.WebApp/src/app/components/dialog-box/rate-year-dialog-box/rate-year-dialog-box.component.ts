import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-rate-year-dialog-box',
  templateUrl: './rate-year-dialog-box.component.html',
  styleUrls: ['./rate-year-dialog-box.component.scss']
})
export class RateYearDialogBoxComponent implements OnInit {

  @Input()
  visible = false;

  @Output()
  isVisibleEvent = new EventEmitter<boolean>();

  @Output()
  generateClickEvent = new EventEmitter<boolean>();

  rate: number = 1;

  constructor() { }

  ngOnInit(): void {
  }

  onCloseClick() {
    this.visible = false;
    this.isVisibleEvent.emit(false);
  }

  onStarRateClick(rate: number) {
    this.rate = rate;
  }

  onGenerateClick() {
    this.visible = false;
    this.isVisibleEvent.emit(false);
    this.generateClickEvent.emit(false);
  }

  onEditClick() {
    this.visible = false;
    this.isVisibleEvent.emit(false);
  }

}
