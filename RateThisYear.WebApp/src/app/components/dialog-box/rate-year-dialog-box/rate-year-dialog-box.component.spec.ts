import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RateYearDialogBoxComponent } from './rate-year-dialog-box.component';

describe('RateYearDialogBoxComponent', () => {
  let component: RateYearDialogBoxComponent;
  let fixture: ComponentFixture<RateYearDialogBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RateYearDialogBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RateYearDialogBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
