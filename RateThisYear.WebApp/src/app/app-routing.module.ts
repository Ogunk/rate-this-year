import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Importing components for routing
import { HomeComponent } from './pages/home/home.component';
import { TakeStockComponent } from './pages/take-stock/take-stock.component';

const routes: Routes = [
  {path : '', component: HomeComponent},
  {path : 'take-stock', component: TakeStockComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
