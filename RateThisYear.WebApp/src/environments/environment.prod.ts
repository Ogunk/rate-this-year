export const environment = {
  production: true,
  microservices : {
    year:{
      baseUrl: 'https://api-year.ratethisyear.io'
    }
  }
};
