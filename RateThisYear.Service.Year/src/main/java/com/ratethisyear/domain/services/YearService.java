package com.ratethisyear.domain.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.ratethisyear.domain.entities.Year;
import com.ratethisyear.domain.interfaces.IManageYear;
import com.ratethisyear.domain.interfaces.IYearService;

@Service
public class YearService implements IYearService{
	
	private final IManageYear yearManager;
	
	@Autowired
	public YearService(IManageYear yearManager) {
		this.yearManager = yearManager;
	}

	@Async
	public Year createNewYear(Year year) {
		return this.yearManager.createNewYear(year); 
	}

	@Async
	public Optional<Year> getYearById(String yearId) {
		return this.yearManager.getYearById(yearId);
	}

}
