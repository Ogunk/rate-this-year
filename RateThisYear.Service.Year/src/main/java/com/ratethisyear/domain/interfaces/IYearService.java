package com.ratethisyear.domain.interfaces;

import java.util.Optional;

import com.ratethisyear.domain.entities.Year;

public interface IYearService {
	
	Year createNewYear(Year year);
	
	Optional<Year> getYearById(String yearId);

}
