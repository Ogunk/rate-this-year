package com.ratethisyear.domain.entities;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Years")
public class Year {
	
	@Id
	private String id;

	@NotEmpty(message="The good events list can't be empty")
	private List<String> goodEvents;
	
	@NotEmpty(message="The bad events list can't be empty")
	private List<String> badEvents;
	
	@Min(0)
	@Max(10)
	@NotNull(message="The year must have a rate")
	private Integer rate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getGoodEvents() {
		return goodEvents;
	}

	public void setGoodEvents(List<String> goodEvents) {
		this.goodEvents = goodEvents;
	}

	public List<String> getBadEvents() {
		return badEvents;
	}

	public void setBadEvents(List<String> badEvents) {
		this.badEvents = badEvents;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

}
