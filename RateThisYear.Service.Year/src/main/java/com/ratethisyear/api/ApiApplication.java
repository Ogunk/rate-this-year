package com.ratethisyear.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@ComponentScan({"com.ratethisyear.api", "com.ratethisyear.domain", "com.ratethisyear.infrastructure"})
@EntityScan({"com.ratethisyear.domain"})
@EnableMongoRepositories("com.ratethisyear.infrastructure")
public class ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}

}
