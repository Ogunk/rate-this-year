package com.ratethisyear.api.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ratethisyear.domain.entities.Year;
import com.ratethisyear.domain.interfaces.IYearService;

@RestController
@RequestMapping("/api/v1/year")
public class YearController {
	
	@Autowired
	private final IYearService yearService;
	
	@Autowired
	public YearController(IYearService yearService) {
		this.yearService = yearService;
	}
	
	// Adding new year to the database
	@PostMapping()
	public Year createNewYear(@Valid @RequestBody Year year) {
		return this.yearService.createNewYear(year);
	}
	
	// Getting a year
	@GetMapping("/{yearId}")
	public Optional<Year> getYearById(@PathVariable String yearId) {
		return this.yearService.getYearById(yearId);
	}

}
