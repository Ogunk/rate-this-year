package com.ratethisyear.infrastructure.adapters;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ratethisyear.domain.entities.Year;
import com.ratethisyear.domain.interfaces.IManageYear;
import com.ratethisyear.infrastructure.repositories.YearRepository;

@Component
public class YearAdapter implements IManageYear{
	
	private final YearRepository yearRepository;
	
	@Autowired
	public YearAdapter(YearRepository yearRepository) {
		this.yearRepository = yearRepository;
	}

	public Year createNewYear(Year year) {
		return this.yearRepository.save(year);
	}

	public Optional<Year> getYearById(String yearId) {
		return this.yearRepository.findById(yearId);
	}

}
