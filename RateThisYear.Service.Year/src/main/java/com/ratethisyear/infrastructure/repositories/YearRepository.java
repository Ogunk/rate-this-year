package com.ratethisyear.infrastructure.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ratethisyear.domain.entities.Year;

@Repository
public interface YearRepository extends MongoRepository<Year, String>{
}
